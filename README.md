# JIRA Build Tools
 
This jar contains helpers to maintain a common JIRA code style as well as common JIRA static analysis tools.  For example
this may contain checkstyle rules that other projects can use.
 
## How to run this plugin
 
From the top level directory, run this command:
 
```
mvn clean install -DskipTests
```
 

## Releasing a new version
 
Simply run:

```
mvn release:prepare release:perform
```
 
## Issue tracking
 
Found a new issue that could be fixed later? 
None yet...